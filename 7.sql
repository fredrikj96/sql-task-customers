SELECT country, COUNT(country)

FROM customer

GROUP BY (customer.country)

ORDER BY COUNT DESC;
