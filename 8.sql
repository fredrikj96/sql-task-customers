SELECT customer.customer_id, first_name, last_name, SUM (total)
FROM customer

INNER JOIN invoice ON invoice.customer_id = customer.customer_id

GROUP BY customer.customer_id

ORDER BY SUM DESC;
